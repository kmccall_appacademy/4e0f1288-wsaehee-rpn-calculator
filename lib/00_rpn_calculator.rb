class RPNCalculator
  #does not need initialize
  #should create an array
  def initialize
    @nums = []
  end

  def value
    @value
  end

  def push(int)
    @nums.push(int)
  end

  def plus
    raise_error
    first = @nums.pop
    second = @nums.pop
    @value = first + second
    self.push(@value)
  end

  def minus
    raise_error
    second = @nums.pop
    first = @nums.pop
    @value = first - second
    self.push(@value)
  end

  def times
    raise_error
    second = @nums.pop
    first = @nums.pop
    @value = first * second
    self.push(@value)
  end

  def divide
    raise_error
    second = @nums.pop
    first = @nums.pop
    @value = first/second.to_f
    self.push(@value)
  end

  def tokens(string)
    string.split.map do |token|
      "+-/*".count(token) > 0 ? token.to_sym : token.to_i
    end
  end

  def evaluate(string)
    tokens = tokens(string)
    symbol = []
    tokens.each do |token|
      if token.class == Fixnum
        @nums.push(token)
      else
        operand_to_method(token)
        puts value
      end
    end
    @value
  end

  def operand_to_method(symbol)
    case symbol
    when :+
      self.plus
    when :-
      self.minus
    when :*
      self.times
    when :/
      self.divide
    end
  end

  def raise_error
    if @nums.length < 1
      raise "calculator is empty"
    end
  end

end

calc = RPNCalculator.new
# calc.push(2)
# calc.push(18)
# puts calc.plus

puts calc.evaluate("1 2 3 * +")
puts calc.value
